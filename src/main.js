// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import store from './store/index'
import DateFilter from './filters/date'
import AlertCmp from './components/shared/Alert.vue'

import EditMeetupDetailsDialog from './components/meetup/edit/EditMeetupDetailsDialog.vue'
import EditMeetupDateDialog from './components/meetup/edit/EditMeetupDateDialog'
import EditMeetupTimeDialog from './components/meetup/edit/EditMeetupTimeDialog'
import RegisterDialog from './components/meetup/registration/RegisterDialog'

import EditMatchmakingDetailsDialog from './components/matchmaking/edit/EditMatchmakingDetailsDialog.vue'
import EditMatchmakingDateDialog from './components/matchmaking/edit/EditMatchmakingDateDialog'
import EditMatchmakingTimeDialog from './components/matchmaking/edit/EditMatchmakingTimeDialog'
import RegisterMatchmakingDialog from './components/matchmaking/registration/RegisterMatchmakingDialog'

Vue.use(Vuetify)
Vue.component('app-alert', AlertCmp)
Vue.component('app-edit-meetup-details-dialog', EditMeetupDetailsDialog)
Vue.component('app-edit-meetup-date-dialog', EditMeetupDateDialog)
Vue.component('app-edit-meetup-time-dialog', EditMeetupTimeDialog)
Vue.component('app-meetup-register-dialog', RegisterDialog)

Vue.component('app-edit-matchmaking-details-dialog', EditMatchmakingDetailsDialog)
Vue.component('app-edit-matchmaking-date-dialog', EditMatchmakingDateDialog)
Vue.component('app-edit-matchmaking-time-dialog', EditMatchmakingTimeDialog)
Vue.component('app-matchmaking-register-dialog', RegisterMatchmakingDialog)

Vue.config.productionTip = false

Vue.filter('date', DateFilter)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBkBXzU0D4afsxQiIAf90hNN3LJ-yL-MJg',
      authDomain: 'easy-level.firebaseapp.com',
      databaseURL: 'https://easy-level.firebaseio.com',
      projectId: 'easy-level',
      storageBucket: 'easy-level.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log(user)
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('fetchUserData')
      }
    })
    this.$store.dispatch('loadMeetups')
    this.$store.dispatch('loadMatchmakings')
  }
})
